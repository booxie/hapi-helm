# coding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'hapi/helm/version'

Gem::Specification.new do |spec|
  spec.name          = 'hapi-helm'
  spec.version       = Hapi::Helm::VERSION
  spec.authors       = ['Alessio Caiazza']
  spec.email         = ['nolith@abisso.org']

  spec.summary       = 'Auto generated kubernetes helm library'
  spec.description   = 'Auto generated kubernetes helm library'
  spec.homepage      = "http://gitlab.com/nolith/hapi-helm"

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.files += Dir['generated_lib/**/*.rb']
  spec.require_paths = ['lib', 'generated_lib']

  spec.add_runtime_dependency 'grpc', '>= 1.6.6'
  spec.add_development_dependency 'grpc-tools', '>= 1.6.6'
  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'pry', '~> 0.10.4'
end
