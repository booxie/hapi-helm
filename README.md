# Hapi::Helm

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/hapi/helm`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'hapi-helm'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install hapi-helm

## Usage

This gem only compiles [helm](https://github.com/kubernetes/helm) [gRPC](https://grpc.io) interface into ruby code.

In order to reach `tiller` daemon it requires a kubernetes [port forwarding](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/).

```sh
# finding tiller pod name
$ kubectl get pods
NAME                            READY     STATUS    RESTARTS   AGE
production-2926023916-70095     1/1       Running   0          26d
production-2926023916-8jtx6     1/1       Running   0          26d
production-2926023916-8vj46     1/1       Running   0          26d
production-2926023916-m1nmp     1/1       Running   0          26d
production-2926023916-v29gj     1/1       Running   0          26d
staging-1489211614-tswq6        1/1       Running   0          26d
tiller-deploy-535128245-v0f27   1/1       Running   0          32d

# starting a local tunnel
$ kubectl port-forward tiller-deploy-535128245-v0f27 44134
Forwarding from 127.0.0.1:44134 -> 44134
Forwarding from [::1]:44134 -> 44134
```

Leave `kubectl port-forward` running and execute your ruby code.

```ruby
stub = Hapi::Services::Tiller::ReleaseService::Stub.new('localhost:44134', :this_channel_is_insecure)
req = Hapi::Services::Tiller::GetVersionRequest.new

stub.get_version(req)
#=> <Hapi::Services::Tiller::GetVersionResponse: Version: <Hapi::Version::Version: sem_ver: "v2.6.1", git_commit: "bbc1f71dc03afc5f00c6ac84b9308f8ecb4f39ac", git_tree_state: "clean">>
```
## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/nolith/hapi-helm.
