require 'bundler/gem_tasks'
require 'rspec/core/rake_task'

PROTO_PATH = 'vendor/helm/_proto'.freeze
GENERATED_LIB = 'generated_lib'.freeze

PROTO_FILES = FileList[PROTO_PATH + '/**/*.proto']
GENERATED_FILES = PROTO_FILES.pathmap("%{^#{PROTO_PATH}/,#{GENERATED_LIB}/}X_pb.rb")

# Helpers
def source_for_pb(pb_file)
  PROTO_FILES.detect{ |f| f.ext('') + '_pb' == pb_file.pathmap("%{^#{GENERATED_LIB}/,#{PROTO_PATH}/}X") }
end

def protoc(source)
  sh "bundle exec grpc_tools_ruby_protoc -I #{PROTO_PATH} --ruby_out=#{GENERATED_LIB} --grpc_out=#{GENERATED_LIB} #{source}"
end

# Tasks
CLOBBER.include(GENERATED_LIB)
RSpec::Core::RakeTask.new(:spec)

task default: :spec

task generate: GENERATED_FILES

file GENERATED_LIB do
  mkdir_p GENERATED_LIB
end

rule '_pb.rb' => [->(f){ source_for_pb(f) }, GENERATED_LIB] do |t|
  protoc(t.source)
end

task build: :generate
task spec: :generate